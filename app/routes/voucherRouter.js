//khai báo thư viện
const express = require("express");

// khai báo hoặc import voucherMiddleware 
const {
    deleteOneVoucherByIdMiddleware,
    putOneVoucherByIdMiddleware,
    postOneVoucherMiddleware,
    getOneVoucherByIdMiddleware,
    getAllVouchersMiddleware
} = require('../middlewares/voucherMiddleware');

//tạo rounter
const voucherRouter = express.Router();

//--------dùng drinkRounter đến các đường dẫn trong postMan--------
// voucherRouter cho all Vouchers 
voucherRouter.get('/vouchers/',getAllVouchersMiddleware, (req,res) => {
    res.json({
        message: 'get All vouchers'
    });
})

// voucherRouter cho get one voucher 
voucherRouter.get('/vouchers/:voucherId',getOneVoucherByIdMiddleware, (req,res) => {
    let voucherId = req.params.voucherId;
    res.json({
        message: `get One Voucher Id = ${voucherId}`
    });
})

//voucherROuter cho update one voucher
voucherRouter.put('/vouchers/:voucherId',putOneVoucherByIdMiddleware, (req,res) => {
    let voucherId = req.params.voucherId;
    res.json({
        message: `update One Voucher Id = ${voucherId}`
    });
})

//voucherROuter cho delete one voucher
voucherRouter.delete('/vouchers/:voucherId',deleteOneVoucherByIdMiddleware, (req,res) => {
    let voucherId = req.params.voucherId;
    res.json({
        message: `delete One Voucher Id = ${voucherId}`
    });
})

//voucherROuter cho create one voucher
voucherRouter.post('/vouchers/',postOneVoucherMiddleware, (req,res) => {
   
    res.json({
        message: `create One Voucher`
    });
})

//export rounter để chạy trên fle index.js
module.exports = {voucherRouter}
