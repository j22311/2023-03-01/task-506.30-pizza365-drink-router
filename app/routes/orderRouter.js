//khai báo thư viện
const express = require("express");

// khai báo hoặc import drinkMiddleware 
const {
    deleteOneOrderByIdMiddleware,
    putOneOrderByIdMiddleware,
    postOneOrderMiddleware,
    getOneOrderByIdMiddleware,
    getAllOrdersMiddleware
} = require ('../middlewares/orderMiddleware');

//tạo rounter
const orderRouter = express.Router();

//--------dùng orderRouter đến các đường dẫn trong postMan--------
//orderRouter cho get All
orderRouter.get('/orders/',getAllOrdersMiddleware, (req,res) => {
    res.json({
        message: 'get All orders'
    });
})

//orderRouter cho post one
orderRouter.post('/orders/',postOneOrderMiddleware, (req,res) => {
    res.json({
        message: 'create one orders'
    });
})

//orderRouter cho update one
orderRouter.put('/orders/:orderId',putOneOrderByIdMiddleware, (req,res) => {
    const orderId = req.params.orderId;
    res.json({
        message: `update one order id= ${orderId}`
    });
})

//orderRouter cho get one
orderRouter.get('/orders/:orderId',getOneOrderByIdMiddleware, (req,res) => {
    const orderId = req.params.orderId;
    res.json({
        message: `get one order id= ${orderId}`
    });
})

//orderRouter cho delete one
orderRouter.delete('/orders/:orderId',deleteOneOrderByIdMiddleware, (req,res) => {
    const orderId = req.params.orderId;
    res.json({
        message: `delete one order id= ${orderId}`
    });
})

module.exports = {orderRouter}

