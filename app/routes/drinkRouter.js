//khai báo thư viện
const express = require("express");

// khai báo hoặc import drinkMiddleware 
const {
    deleteOneDrinksByIdMiddleware,
    putOneDrinksByIdMiddleware,
    postOneDrinksMiddleware,
    getOneDrinksByIdMiddleware,
    getAllDrinksMiddleware
} = require ('../middlewares/drinkMiddleware');

//tạo rounter
const drinkRouter = express.Router();

//--------dùng drinkRounter đến các đường dẫn trong postMan--------
//drinkROunter cho get All
drinkRouter.get('/drinks/',getAllDrinksMiddleware, (req,res) => {
    res.json({
        message: 'get All Drinks'
    });
})

//drinkROunter cho get one
drinkRouter.get('/drinks/:drinkId',getOneDrinksByIdMiddleware, (req,res) => {
    let drinkId = req.params.drinkId;
    res.json({
        message: `get One drink Id= ${drinkId}`
    });
})

//drinkROunter cho put one
drinkRouter.put('/drinks/:drinkId',putOneDrinksByIdMiddleware, (req,res) => {
    let drinkId = req.params.drinkId;
    res.json({
        message: `update One drink Id= ${drinkId}`
    });
})

//drinkROunter cho delete one
drinkRouter.delete('/drinks/:drinkId',deleteOneDrinksByIdMiddleware, (req,res) => {
    let drinkId = req.params.drinkId;
    res.json({
        message: `delete One drink Id= ${drinkId}`
    });
})

//drinkROunter cho create one
drinkRouter.post('/drinks/',postOneDrinksMiddleware, (req,res) => {
   
    res.json({
        message: `create One drink`
    });
})
//export rounter để chạy trên fle index.js
module.exports = {
    drinkRouter
}