//khai báo thư viện
const express = require("express");

// khai báo hoặc import drinkMiddleware 
const {
    deleteOneUserByIdMiddleware,
    putOneUserByIdMiddleware,
    postOneUserMiddleware,
    getOneUserByIdMiddleware,
    getAllUsersMiddleware
} = require ('../middlewares/userMiddleware');

//tạo rounter
const userRouter = express.Router();

//--------dùng userRouter đến các đường dẫn trong postMan--------
//userRouter cho get All
userRouter.get('/users/',getAllUsersMiddleware, (req,res) => {
    res.json({
        message: 'get All users'
    });
})

//userRouter cho post one
userRouter.post('/users/',postOneUserMiddleware, (req,res) => {
    res.json({
        message: 'create one users'
    });
})

//userRouter cho update one
userRouter.put('/users/:userId',putOneUserByIdMiddleware, (req,res) => {
    const userId = req.params.userId;
    res.json({
        message: `update one user id= ${userId}`
    });
})

//userRouter cho get one
userRouter.get('/users/:userId',getOneUserByIdMiddleware, (req,res) => {
    const userId = req.params.userId;
    res.json({
        message: `get one user id= ${userId}`
    });
})

//userRouter cho delete one
userRouter.delete('/users/:userId',deleteOneUserByIdMiddleware, (req,res) => {
    const userId = req.params.userId;
    res.json({
        message: `delete one user id= ${userId}`
    });
})

module.exports = {userRouter}

