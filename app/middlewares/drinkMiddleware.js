const getAllDrinksMiddleware = (req,res,next) => {
    console.log("get All Drinks (middleware)");
    next();
}

const getOneDrinksByIdMiddleware = (req,res,next) => {
    console.log("get One Drinks (middleware)");
    next();
}

const postOneDrinksMiddleware = (req,res,next) => {
    console.log("post AOne Drinks (middleware)");
    next();
}

const putOneDrinksByIdMiddleware = (req,res,next) => {
    console.log("put One Drinks (middleware)");
    next();
}

const deleteOneDrinksByIdMiddleware = (req,res,next) => {
    console.log("get All Drinks (middleware)");
    next();
}

module.exports = {
    deleteOneDrinksByIdMiddleware,
    putOneDrinksByIdMiddleware,
    postOneDrinksMiddleware,
    getOneDrinksByIdMiddleware,
    getAllDrinksMiddleware
}